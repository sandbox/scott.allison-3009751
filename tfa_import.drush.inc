<?php

/**
 * @file
 * Drush command defintions for the TFA Import module.
 */

use Drupal\tfa_import\Form\TfaImport;

/**
 * Implements hook_drush_command().
 */
function tfa_import_drush_command() {
  $items = array();
  $items['tfai'] = [
    'description' => 'Trigger an import of nodes or users',
    'arguments' => [
      'type' => 'Allowed types are "nodes" or "users"',
      'status' => 'Import entities as published/active (1) or unpublished/blocked (0)',
      'limit' => '# of records to import',
      'roles' => 'Assign roles to the imported users. Separate multiple roles with a comma',
    ],
  ];
  return $items;
}

/**
* Validation callback for the tfai drush command
*/
function drush_tfa_import_tfai_validate($type='',$status=0,$limit=10,$roles='') {
  if (empty($type)) {
    return drush_set_error('INVALID_TYPE', dt('Type is required'));
  }
  if (!is_numeric($status)) {
    return drush_set_error('INVALID_STATUS', dt('Status should be either 0 or 1'));
  }
  if (!is_numeric($limit) || $limit <= 0) {
    return drush_set_error('INVALID_LIMIT', dt('Limit must be a positive integer'));
  }
  if (!empty($roles)) {
    $valid_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    $roles = explode(',', $roles);
    foreach($roles as $role) {
      if (empty($valid_roles[$role])) {
        return drush_set_error('INVALID_ROLE', dt('Role @role is invalid', ['@role' => $role]));
      }
    }
  }

  $allowed_types = [
    'nodes',
    'users',
  ];
  if (!in_array($type, $allowed_types)) {
    return drush_set_error('INVALID_TYPE', dt('Type is invalid'));
  }
}

/**
* Callback for the tfai drush command
*/
function drush_tfa_import_tfai($type='',$status=0,$limit=10,$roles='') {
  if (drush_confirm(dt('Are you sure you wish to import @type as @active @roles @limit?', ['@type' => $type, '@active' => $status ? 'active' : 'inactive', '@roles' => !empty($roles) ? 'with role(s) ' . $roles : '', '@limit' => 'with a limit of ' . $limit]))) {
    drush_print('Starting import...');
    $message = [];
    $context = [];
    switch ($type) {
      case 'nodes':
        $posts = TfaImport::getPosts();
        $i=0;
        foreach ($posts as $post) {
          if (empty($post['title']) || TfaImport::UniqueNode($post['title']) == FALSE) {
            continue;
          }
          $node = tfa_import_node($post, $status, $message, $context);
          $i++;
          if ($i >= $limit) {
            break;
          }
        }
        drush_print($i > 0 ? $i . ' nodes imported' : 'No nodes imported, or no unique nodes remaining to be impoted.');
        break;

      case 'users':
        $users = TfaImport::getUsers();
        $i=0;
        foreach ($users as $account) {
          if (empty($account['email']) || TfaImport::UniqueUser($account['email']) == FALSE) {
            continue;
          }
          if (!empty($roles)) {
            $roles = explode(',',$roles);
          }
          $account = tfa_import_user($account, $status, $roles, $message, $context);
          $i++;
          if ($i >= $limit) {
            break;
          }
        }
        drush_print($i > 0 ? $i . ' users imported' : 'No users imported, or no unique users remaining to be imported.');
        break;
    }
  }
}
