Installation
=========================
1. Enable the module
2. Enable the permission "Access TFA import"
3. Go to /admin/config/services/tfa-import


Drush Command
=========================
drush tfai
drush help tfai
