<?php

namespace Drupal\tfa_import\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

class TfaImport extends FormBase {

  public function getFormId() {
    return 'tfa_import';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['type'] = [
      '#type' => 'select',
      '#title' => 'Choose Import',
      '#options' => [
        'nodes' => $this->t('Nodes'),
        'users' => $this->t('Users'),
      ],
    ];
    
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => 'Published/Active',
      '#description' => 'Leave unchecked to import nodes as unpublished nodes, or users as blocked users.',
    ];

    $options = [];
    $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
    foreach ($roles as $key => $role) {
      if ($key == 'anonymous') {
        continue;
      }
      $options[$key] = $role->label();
    }
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#options' => $options,
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'users'],
        ],
      ],
    ];
    $form['roles']['authenticated']['#disabled'] = TRUE;
    $form['roles']['authenticated']['#default_value'] = TRUE;
    
    $form['limit'] = [
      '#type' => 'number',
      '#title' => '# of Records',
      '#description' => 'Enter the number of records to import.',
      '#default_value' => \Drupal::state()->get('tfa_import_limit', 10),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Import',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $type = $form_state->getValues()['type'];
    $status = $form_state->getValues()['status'];
    $roles = $form_state->getValues()['roles'];
    $limit = $form_state->getValues()['limit'];
    $batch = [];
    switch ($type) {
      case 'nodes':
        $batch = $this->importNodes($status, $limit);
        break;

      case 'users':
        $batch = $this->importUsers($status, $roles, $limit);
        break;
    }
    batch_set($batch);
  }

  /**
   * Makes the API call to retrieve a JSON feed of posts
   */
  public function getPosts() {
    $uri = 'https://jsonplaceholder.typicode.com/posts';
    $response = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
    $data = (string) $response->getBody();
    if (empty($data)) {
      return FALSE;
    }
    $posts = Json::decode($data);
    return $posts;
  }

  /**
   * Kicks off the batch process to import a set number of posts from the
   * designated API endpoint.
   *
   * @var int $status
   *  0 for unpublished and 1 for published
   *
   * @var int $limit
   *  The number of records to import
   *
   * @return array $batch
   */
  public function importNodes($status=0, $limit=10) {
    try {
      $posts = $this->getPosts();
      $i=0;
      $operations = [];
      foreach ($posts as $post) {
        if (empty($post['title']) || $this->UniqueNode($post['title']) == FALSE) {
          continue;
        }
        $operations[] = [
          'tfa_import_node',
          [
            $post,
            $status,
            $this->t('Importing @title', ['@title' => $post['title']]),
          ],
        ];
        $i++;
        if ($i >= $limit) {
          break;
        }
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }

    $batch = [
      'title' => $this->t('Importing @num nodes', ['@num' => count($operations)]),
      'operations' => $operations,
      'finished' => 'tfa_import_batch_finished',
    ];
    return $batch;
  }

  /**
   * Finds nodes with matching titles
   *
   * @var string $title
   *  The node title
   *
   * @return boolean
   *  True if the node already exsits, false if it's unique
   */
  public function UniqueNode($title) {
    $query = \Drupal::entityQuery('node')
      ->condition('title', $title);
    $result = $query->execute();
    return empty($result);
  }

  /**
   * Makes the API call to retrieve a JSON feed of users
   */
  public function getUsers() {
    $uri = 'https://jsonplaceholder.typicode.com/users';
    $response = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
    $data = (string) $response->getBody();
    if (empty($data)) {
      return FALSE;
    }
    $users = Json::decode($data);
    return $users;
  }

  /**
   * Kicks off the batch process to import a set number of users from the
   * designated API endpoint.
   *
   * @var int $status
   *  0 for blocked and 1 for active
   *
   * @var int $limit
   *  The number of records to import
   *
   * @return array $batch
   */
  public function importUsers($status=0, $roles=[], $limit=10) {
    try {
      $users = $this->getUsers();
      $i=0;
      $operations = [];
      foreach ($users as $account) {
        if (empty($account['email']) || $this->UniqueUser($account['email']) == FALSE) {
          continue;
        }
        $operations[] = [
          'tfa_import_user',
          [
            $account,
            $status,
            $roles,
            $this->t('Importing @email', ['@email' => $account['email']]),
          ],
        ];
        $i++;
        if ($i >= $limit) {
          break;
        }
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }

    $batch = [
      'title' => $this->t('Importing @num users', ['@num' => count($operations)]),
      'operations' => $operations,
      'finished' => 'tfa_import_batch_finished',
    ];
    return $batch;
  }

  /**
   * Finds users with matching emails
   *
   * @var string $email
   *  The user's email
   *
   * @return boolean
   *  True if the user already exsits, false if it's unique
   */
  public function UniqueUser($email) {
    $user = user_load_by_mail($email);
    return empty($user);
  }
}
